import org.junit.Assert;
import org.junit.Test;
import rigovsky.ivan.calculator.Calculator;

public class CalculatorTest {

    @Test
    public void smallestTest() {
        Calculator calculator = Calculator.getInstance();
        calculator.add(1L);
        calculator.add(2L);
        calculator.add(null);
        Assert.assertEquals(Long.valueOf(1L), calculator.getSmallest());
    }

    @Test
    public void largerTest() {
        Calculator calculator = Calculator.getInstance();
        calculator.add(1L);
        calculator.add(2L);
        calculator.add(null);
        Assert.assertEquals(Long.valueOf(2L), calculator.getLargest());
    }

    @Test
    public void averageTest() {
        Calculator calculator = Calculator.getInstance();
        calculator.add(1L);
        calculator.add(2L);
        calculator.add(null);
        Assert.assertEquals(Float.valueOf(1.5F), calculator.getAverage());
    }
}
