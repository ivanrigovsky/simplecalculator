package rigovsky.ivan.calculator;

/**
 * Interface with specified scenarios.
 */
public interface ICalculator {

    /**
     * Add value to the calculator.
     */
    void add(Long value);

    /**
     * Get the smallest number it has encountered so far.
     */
    Long getSmallest();

    /**
     * Get the largest number it has encountered so far.
     */
    Long getLargest();

    /**
     * Get the average of all numbers it has encountered so far.
     */
    Float getAverage();
}
