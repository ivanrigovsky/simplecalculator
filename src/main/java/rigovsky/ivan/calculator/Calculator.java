package rigovsky.ivan.calculator;

public class Calculator implements ICalculator {

    public static Calculator getInstance() {
        return new Calculator();
    }

    private Calculator() {}

    private Long smallest = null;
    private Long largest = null;
    private long sum = 0;
    private int count;

    @Override
    public void add(Long value) {
        checkLargest(value);
        checkSmallest(value);
        addToSum(value);
    }

    @Override
    public Long getSmallest() {
        return smallest;
    }

    @Override
    public Long getLargest() {
        return largest;
    }

    @Override
    public Float getAverage() {
        if (count > 0) {
            float avg = sum;
            return avg / count;
        }
        return null;
    }

    private void checkSmallest(Long value) {
        if (smallest == null || (value != null && value < smallest)) {
            smallest = value;
        }
    }

    private void checkLargest(Long value) {
        if (largest == null || (value != null && value > largest)) {
            largest = value;
        }
    }

    private void addToSum(Long value) {
        if (value != null) {
            sum += value;
            count++;
        }
    }
}
