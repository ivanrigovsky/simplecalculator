import rigovsky.ivan.calculator.Calculator;

public class Runner {

    public static void main(String[] args) {
        Calculator calculator = Calculator.getInstance();

        calculator.add(1L);
        calculator.add(2L);
        calculator.add(3L);
        calculator.add(4L);
        calculator.add(null);

        System.out.println("Average " + calculator.getAverage());
        System.out.println("Smallest " + calculator.getSmallest());
        System.out.println("Largest " + calculator.getLargest());
    }
}
